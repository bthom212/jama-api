import os, sys
from py_jama_rest_client.client import JamaClient

jama_url = 'https://jama.honeywell.com'


jama_client = JamaClient(jama_url, credentials=(jama_api_username, jama_api_password))

for cycle in sys.argv[1:]:
    print(cycle)
    TestCycle = jama_client.get_testruns(cycle)
    for TestRun in TestCycle:
        try:
            TR_key = TestRun['id']
            TestCase = TestRun['fields']['testCase']
            create = jama_client.post_relationship(TestCase, TR_key)
        except:
            pass