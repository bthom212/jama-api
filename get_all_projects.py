import os, xlsxwriter
from py_jama_rest_client.client import JamaClient

# Setup your Jama instance url, username, and password.
# You may use environment variables, or enter your information directly.
# Reminder: Follow your companies security policies for storing passwords.


# Create the JamaClient
jama_client = JamaClient(jama_url, credentials=(jama_api_username, jama_api_password))

workbook = xlsxwriter.Workbook('Trace_Matrix.xlsx')
worksheet = workbook.add_worksheet()

row = 0
col = 4

# Get the list of projects from Jama
# The client will return to us a JSON array of Projects, where each project is a JSON object.
TestCycle = jama_client.get_testruns(33104289)

#

# Print the data out for each project.
for TestRun in TestCycle:
    project_name = TestRun['fields']['documentKey']
    print('\n---------------' + project_name + '---------------')
    worksheet.write(row, col, project_name)
   
    TestCase = TestRun['fields']['testCase']
    print(TestCase)

    TestCaseObj = jama_client.get_item(TestCase)
    
    TC_Num = TestCaseObj['fields']['documentKey']
    worksheet.write(row, col - 1, TC_Num)
    TC_Name = TestCaseObj['fields']['name']
    print(TC_Num);
    print(TC_Name);

    UpstreamSRS = jama_client.get_items_upstream_relationships(TestCase)
    SRSOBJIDStr = ""
    for SRS in UpstreamSRS:
        SRSid = SRS['fromItem']
        print(SRSid)
        
        SRSObj = jama_client.get_item(SRSid)
        SRSObjID = SRSObj['fields']['documentKey']
        SRSOBJIDStr = SRSOBJIDStr + SRSObjID + "/n"
        print(SRSObjID)
        SRSName = SRSObj['fields']['name']
        print(SRSName)
        
        UpstreamMRS = jama_client.get_items_upstream_relationships(SRSid)
        MRSObjIDStr = ""
        for MRS in UpstreamMRS:
            MRSid = MRS['fromItem']
            print(MRSid)
            
            MRSObj = jama_client.get_item(MRSid)
            MRSObjID = MRSObj['fields']['documentKey']
            MRSObjIDStr = MRSObjIDStr + MRSObjID + "/n"
            print(MRSObjID)
            MRSName = MRSObj['fields']['name']
            print(MRSName)
            
    worksheet.write(row, col - 2, SRSOBJIDStr)
    worksheet.write(row, col - 3, MRSObjIDStr)
    row = row + 1
workbook.close()